from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpRequest
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm

# Create your views here.
def my_list(request: HttpRequest):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request: HttpRequest, id):
    todo_detail = TodoList.objects.get(id=id)
    context = {"todo_detail": todo_detail}
    return render(request, "todos/detail.html", context)

def create_list(request: HttpRequest):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect('todo_list_detail', todolist.id)
    else:
        form = TodoListForm()
    context = {"form":form}
    return render(request, "todos/create.html", context)

def todo_list_update(request:HttpRequest):
    todolist: TodoList = get_object_or_404(TodoList, id=id)
    if request.method == "GET":
        form = TodoListForm(instance = todolist)
    elif request.method == "POST":
        form = TodoListForm(request.POST, instance = todolist)
        if form.is_valid():
            form.save()
    context = {
        "form": form,
        "todolist": todolist
    }
    return render(request, "todos/create.html", context)
