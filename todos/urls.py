from django.urls import path
from todos.views import my_list, todo_list_detail, create_list, todo_list_update

urlpatterns = [
    path("", my_list, name = "todo_list_list"),
    path("<int:id>/", todo_list_detail, name = "todo_list_detail"),
    path("create/", create_list, name = "todo_list_create"),
    path("<int:id>/edit/", todo_list_update, name = "todo_list_update")
]
